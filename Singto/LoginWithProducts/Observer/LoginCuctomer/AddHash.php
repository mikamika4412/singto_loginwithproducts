<?php

namespace Singto\LoginWithProducts\Observer\LoginCuctomer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\InputMismatchException;
use Magento\Setup\Exception;

class AddHash implements ObserverInterface
{

    /**
     * @var CustomerRepositoryInterface
     */
    private CustomerRepositoryInterface $customerRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @param CustomerRepositoryInterface $customerRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @param Observer $observer
     * @return void
     * @throws InputException
     * @throws LocalizedException
     * @throws InputMismatchException
     */
    public function execute(Observer $observer)
    {
        $customerSearchResults = $this->customerRepository->getList($this->searchCriteriaBuilder->create())->getItems();
        foreach ($customerSearchResults as $item) {
            try {
                if ($item->getCustomAttributes('hash') == null) {
                    $item->setCustomAttribute('hash', bin2hex(random_bytes(16)));
                    $this->customerRepository->save($item);
                }
            } catch (Exception $e) {
                echo $e;
            }
        }
    }
}


