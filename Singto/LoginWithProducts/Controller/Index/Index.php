<?php

namespace Singto\LoginWithProducts\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Result\PageFactory;
use Magento\Quote\Api\CartRepositoryInterface;
use Exception;
use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Checkout\Model\SessionFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Customer\Model\Session;
use Magento\Framework\App\ResponseInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\Action\Action;
use Magento\Store\Model\StoreManagerInterface;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var Magento\CatalogInventory\Api\StockStateInterface
     */
    protected $stockState;

    /** @var SessionFactory */
    protected $checkoutSession;

    /** @var CartRepositoryInterface */
    protected $cartRepository;

    /** @var ProductRepositoryInterface */
    protected $productRepository;

    /** @var Json */
    protected $json;

    /** @var Configurable */
    protected $configurableType;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;
    /**
     * @var Product
     */
    protected $_product;

    /**
     * @var bool
     */
    protected bool $notEnoughQty = false;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ProductRepositoryInterface $productRepository
     * @param ProductFactory $productFactory
     * @param Session $session
     * @param Product $product
     * @param StockStateInterface $stockState
     * @param Json $json
     * @param SessionFactory $checkoutSession
     * @param CartRepositoryInterface $cartRepository
     * @param Configurable $configurableType
     * @param CustomerFactory $customerFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ProductRepositoryInterface $productRepository,
        ProductFactory $productFactory,
        Session $session,
        Product $product,
        StockStateInterface $stockState,
        Json $json,
        SessionFactory $checkoutSession,
        CartRepositoryInterface $cartRepository,
        Configurable $configurableType,
        CustomerFactory $customerFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->_customerSession = $session;
        $this->_product = $product;
        $this->stockState = $stockState;
        $this->checkoutSession = $checkoutSession;
        $this->cartRepository = $cartRepository;
        $this->json = $json;
        $this->configurableType = $configurableType;
        $this->customerFactory = $customerFactory;
        parent::__construct($context);
    }

//        http://msp2.com/singto/?hash=74cb2995e44c0b7862d24e01cb46ebba&tovar=24-MB02,1;24-MB06,2;24-MB04,3

    /**
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $hash = $this->_request->getParam('hash');
        $tovar = $this->_request->getParam('tovar');
        $this->entranceHash($hash);
        if (!$this->_customerSession->isLoggedIn()) {
            return $this->redirect('/');
        }
        $session = $this->checkoutSession->create();
        $quote = $session->getQuote();

        try {
            $this->addProdToCart($tovar, $quote);
        } catch (Exception $e) {
            return $this->redirect('/');
        }

        if ($this->notEnoughQty) {
            return $this->redirect('/');
        }
        $this->cartRepository->save($quote);
        $session->replaceQuote($quote)->unsLastRealOrderId();

        return $this->redirect('checkout/cart');
    }

    /**
     * @param $tovar
     * @param $quote
     * @return bool|void
     * @throws NoSuchEntityException
     */
    public function addProdToCart($tovar, $quote)
    {
        $getSkuAndQtyAr = explode(';', $tovar);
        foreach ($getSkuAndQtyAr as $value) {
            $getSkuQty = explode(',', $value);
            $Sku = $getSkuQty[0];
            $Qty = $getSkuQty[1];
            $existProduct = $this->productRepository->get($Sku);
            $existQty = !empty($existProduct->getId()) ? $this->getStockQty($existProduct->getId()) : 0;
            try {
                if ($existProduct && $Qty <= $existQty) {
                    $quote->addProduct($existProduct, $Qty);
                } else {
                    return $this->notEnoughQty = true;
                }
            } catch (\Magento\Setup\Exception $e) {
                echo $e;
            }
        }
    }

    /**
     * @param $path
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function redirect($path)
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath($path);
        return $resultRedirect;
    }

    /**
     * @param $productId
     * @return float
     */
    public function getStockQty($productId)
    {
        return $this->stockState->getStockQty($productId);
    }

    /**
     * @param $hash
     * @return mixed
     */
    public function entranceHash($hash)
    {
        $collection = $this->customerFactory->create()->getCollection()->addAttributeToSelect("*")
            ->addAttributeToFilter("hash", $hash)->load();
        $customerData = $collection->getData();
        $customerId = $customerData[0]['entity_id'];
        $this->_customerSession->loginById($customerId);
    }

}
